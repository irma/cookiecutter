---
title: {{ cookiecutter.__project_name }}
name: {{ cookiecutter.__full_name }}
theme: 
    name: {{ cookiecutter.opla_theme }}
data:
    - img
    - cv.pdf
footer:
    contact:
      - "<{{ cookiecutter.email }}>"
      - "A dummy address, 75000 Paris, France"
      - "+33 dummy number"
    social:
      - icon: gitlab
        url: {{ cookiecutter.__git_remote_server_url }}/{{ cookiecutter.git_remote_username }}
        color: white
---

![{{ cookiecutter.__full_name }}'s portrait](img/portrait.jpg){: .responsive-img .z-depth-3 .left .mr-6 width="185px"}

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.

You can download my curriculum vitae [here](cv.pdf).

## Research Interests

My research interests span a wide range of topics in mathematics and physics, including:

- Partial differential equations
- Fluid dynamics
- Celestial mechanics
- Philosophy of science
- History of mathematics
- Music theory
- Social sciences
- Literature
- Fine arts
- ...


## Publications

### Preprints

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.

### Articles

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.


## Seminars and Talks

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.

## Teaching

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Lorem ipsum dolor sit amet, consectetur adipiscing elit.