# {{ cookiecutter.__project_name }}

{{ cookiecutter.__project_short_description }} published at <{{ cookiecutter.__git_remote_pages_url }}>.

See [OPLA documentation](https://irma.pages.math.unistra.fr/opla/) for details.
