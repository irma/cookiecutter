# Cookiecutter templates

## Pre-requisites

A Python 3.8+ environment is required to run `cookiecutter` with `pipx`.

### Install `pipx`

Install [`pipx`](https://pipx.pypa.io/stable/) using `pip`:

```bash
pip install pipx
pipx ensurepath
```

### Install `cookiecutter`

Install [`cookiecutter`](https://cookiecutter.readthedocs.io/en/stable/index.html) using `pipx`:

```bash
pipx install cookiecutter
```

## Instanciante `opla` template

```bash
cookiecutter https://gitlab.math.unistra.fr/irma/cookiecutter.git --directory opla
```

