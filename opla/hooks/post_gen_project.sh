#!/bin/bash

# Initialize a git repository and 

git init
git add .
git commit -m "First commit for an OPLA website"
git remote add origin {{ cookiecutter.__git_remote_ssh }}
git branch --set-upstream-to=origin/main main
if [ "{{ cookiecutter.push_to_git_remote }}" == "True" ]; then
    # push the code to the remote repository if push_to_git_remote is set to true
    git push origin main
fi
